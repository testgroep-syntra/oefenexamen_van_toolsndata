"""
Opdracht:

Bepalingen:
 - Je moet gebruik maken van de aangeleverde variable(n)
 - Je mag deze variable(n) niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Inlever datum is zondag avond 13 maart 2022 18:00CET
 - Als inlever formaat wordt een git url verwacht die gecloned kan worden

/ 5 ptn 1 - Maak een public repository aan op jouw gitlab account voor dit project
/10 ptn 2 - Gebruik python om de gegeven api url aan te spreken
/20 ptn 3 - Gebruik regex om de volgende data te extracten:
    - Jaar, maand en dag van de created date
    - De provider van de nameservers (bijv voor 'ns3.combell.net' is de provider 'combell')
/15 ptn 4 - Verzamel onderstaande data en output alles als yaml. Een voorbeeld vind je hieronder.
    - Het land van het domein
    - Het ip van het domain
    - De DNS provider van het domein
    - Aparte jaar, maand en dag van de created date


Totaal  /50ptn
"""

""" voorbeeld yaml output

created:
  dag: '18'
  jaar: '2022'
  maand: '02'
ip: 185.162.31.124
land: BE
provider: combell

"""

import requests
import json
import re
import yaml
from pprint import pprint

                # Voor requests ivm opvragen URL
                #in terminal=> python3 -m pip install requests
                #toevoegen => import request
                #JSON formaat
                #toevoegen => import json om response in json file te ontvangen

url = "https://api.domainsdb.info/v1/domains/search?domain=syntra.be"

response = requests.get(url).json()

                #pprint(response)   om de response van de request af te drukken

#   DATUM
 
date = response["domains"][0]['create_date']
regex_date = r"((?P<jaar>\d{4})\-(?P<maand>\d{2})\-(?P<dag>\d{2}))"

for match in re.finditer(regex_date, date):
    datum_dict = match.groupdict()              #opslaan als dictionary

                #print("gecreëerd op:")    
                # print(datum_dict["dag"])
                # print(datum_dict["maand"])
                # print(datum_dict["jaar"])


                # omdraaien volgorde dictionary
  
                # reversed_datum_dict = dict(reversed(datum_dict.items()))
                # print(reversed_datum_dict)
                # print(datum_dict)

                # output
                # {'jaar': '2023', 'maand': '10', 'dag': '20'}
                # {'dag': '20', 'maand': '10', 'jaar': '2023'}

                # for key, value in reversed_datum_dict.items():
                #     print(f"\t{key}:\t {value}")
    
#   PROVIDER           

provider = response["domains"][0]["NS"][0]

                # print(provider)
                # output:
                # ns3.belnet.be

regex_provider = r"(\w{6})"                                
match_provider = re.findall(regex_provider,provider)

                    #regex_provider = r"((?P<provider>\w+)\.\w+$)"
                    # for match in re.finditer(regex_provider, provider, flags=re.MULTILINE):
                    #     provider_dict = match.groupdict()  #opslaan als dictionary

                    #enkel_naam_provider = provider_dict["provider"]   #variabele


#   LAND

land = response["domains"][0]["country"]

    #print(land)

#   IP ADRES

ip_adres = response["domains"][0]["A"]
#print(ip_adres)


#   OUTPUT met YAML

                    #terminal => pip install pyyaml
                    #toevoegen : import yaml 

print(" ")
print("\tNu volgt de output")
print("\t------------------")
print
output = { 
"created": datum_dict,
"ip": ip_adres, 
"land": land,
"provider": match_provider 
}

print(yaml.dump(output))


